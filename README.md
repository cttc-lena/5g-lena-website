## Website for 5G-LENA

This repo is the home of the website of 5G-LENA. Some things are different from what you are used to, but this file is here to explain everything, starting from the beginning.

### What is Pelican?

[Pelican](https://blog.getpelican.com/) is a static site generator, written in Python, that requires no database or server-side logic. Which means that starting from text files written in reStructuredText (but also in Markdown, or AsciiDoc formats) you can get the result HTML file. The 'static' part means that the generation occurs once, and the site creator triggers it manually by calling the `pelican` program. There is not PHP or Python backend that will generate new pages when a database is updated! So, keep in mind that you can be as creative as possible, put all the client-side javascript that you wish, but you can't modify it live. What you should do, instead, is to alter the RST files, and then re-generate the HTML files.

### Why it is interesting in our case?
We are developers, and we have a robust git backend. We are inclined to use useful tools that do not change too much the way we produce artifacts. Here is the deal: we can write a website in the same way we create a piece of code, without having to pass from fancy giants like Joomla, Drupal, Wordpress. We are interested in content, not in fanciness. We do not care about plugins, modules, or point-and-click tools that pretend to simplify our life, hiding different complexity levels behind a kid's interface except that the very same tools will create a hell out of what we have, if we merely want to do something that was not thought by the tool's creators. If you are still not convinced, [here](https://mcss.mosra.cz/why/) you can find something more.

### Starting point
Pelican supports Articles (e.g., blog posts) and pages (e.g., “About”, “Projects”, “Contact”). Each final HTML file is created starting from a template, in which the content will be inserted at the generation time. For instance, a simple template that generates a file with a list of users and URLs can be:

```
{% extends "layout.html" %}
{% block body %}
  <ul>
  {% for user in users %}
    <li><a href="{{ user.url }}">{{ user.username }}</a></li>
  {% endfor %}
  </ul>
{% endblock %}
```

Natale has done the process of generating the template (that is the most tricky part), and now is time to generate the content. If you are interested in how these templates works, please take a look to [Jinja](http://jinja.pocoo.org/).

### Generating content
Each page is written in RST format (like the ns-3 manual). The easiest way to deal with it is to just write the content, with the titles and so on, and then saving the file. You will see from the existing pages that there is a special section, in the beginning, in which are listed some properties of the file (e.g., where the file will be saved). In most of the cases, a default section could be:

```
Page title
######

:summary: Page summary

.. role:: dox-dim(dox)
    :class: m-dim

[Content that you will write]
```

The page should be saved under the directory `content/`. In some cases, the page has already been created, and the only thing to do is to fill the content.

### Advanced use
The client-side backed that we are using is `m.css`, hosted [here](https://mcss.mosra.cz/). The framework is, as its webpage says, `a no-non­sense, no-JavaScript CSS frame­work and Pel­i­can theme for con­tent-ori­ent­ed web­sites`. Through the use of CSS only, it creates a grid on which all the elements of the pages are displayed. The concept is to design a responsive site (i.e., that can adapt to different screen sizes) and is very well known ([this](https://www.w3schools.com/css/css_rwd_grid.asp) is the page dedicated to that design pattern in the w3schools website). So, if you are adventurous enough to create a template, surely you will have to follow the [guidelines](https://mcss.mosra.cz/css/grid/) for the users of `m.css`. They explain how to use the various classes to create your perfectly-adaptable template.

Since the templating work is already done, what else you can do? Well, you could format your content to be displayed in some kind of way, instead of using the default "left-to-right, top-to-bottom" method that you will get by not explicitly write a layout. So, before everything, please re-read the guidelines on [m.css grid](https://mcss.mosra.cz/css/grid/). Then, let's see how you can specify the block classes directly in the RST file by looking to an example.

We will analyze the file `teams.rst`, in which we present the team member. 
