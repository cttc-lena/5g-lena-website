Features
########

:summary: High-level overview of feature set offered by 5G-LENA
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: *5G-LENA high-level feature overview*

            * Non-Standalone Architecture (NSA) architecture: 5G RAN and 4G EPC
            * 3GPP 38.901 spatial channel and antenna array models able to simulate 0.5-100 GHz frequency ranges
            * Time Division Duplexing (TDD) and Frequency Division Duplexing (FDD) modes, 
            * Configurable TDD patterns
            * Flexible and automatic configuration of the NR frame structure through multiple numerologies
            * Time-Division Multiple Access (TDMA) and Orthogonal Frequency-Division Multiple Access (OFDMA)-based access with variable transmission time intervals (TTI) and single beam capability
            * Enhanced MAC layer, including flexible MAC schedulers that simultaneously consider time- and frequency-domain resources (resource blocks and OFDM symbols) both for TDMA and OFDMA-based access schemes with variable TTI
            * UpLink (UL) grant-based access scheme with scheduling request
            * 3GPP-compliant UL buffer status reporting
            * NR-compliant processing delays and control timings (N0, N1, N2; K0, K1, K2)
            * Multiple Bandwidth Part (BWP) and component carriers operation
            * NR PHY layer abstraction, considering LDPC codes, MCS Tables 1 and 2 (up to 256-QAM), LDPC base graph selection and NR block segmentation
            * Interference management/coordination (including ICIC and notching)
            * Uplink power control
            * Sounding reference signals (SRS) modeling
            * Realistic beamforming based on SRS-based channel estimates
            * Antennas with multiple-ports and dual-polarized antennas
            * MIMO modeling, supporting two streams through either multiple antenna ports and dual-polarized antennas
            * 3GPP-compliant PMI (precoding matrix indicator) and RI (rank indicator), assuming MMSE-IRC (interference rejection combining) receiver for inter-stream interference suppression
            * CQI/PMI/RI feedback and rank adaptation for MIMO
            * QoS schedulers
            * Helpers to easily manage spectrum configuration
            * Multi-Cell configurations allowing multiple cells with different configurations
            * Radio Environment Map (REM) helpers for the visualization of downlink and uplink (REM) maps
            * Helpers to create different topologies, e.g., hexagonal topology helper and grid topology helper
            * Various helpers that generate traces at different layers of the protocol stack
            * NGMN traffic models: video streaming, voice over IP (VoIP), uplink/downlink gaming, FTP 
            * 3GPP XR traffic models:  XR (AR, VR and CG) and 3GPP FTP Model 1

            Additional extensions include NR-U an NR V2X simulators:

            * NR-U implements NR operation in unlicensed spectrum, through multiple channel access managers, including duty-cycling as well as Listen-Before-Talk (LBT)-based procedures.
            * NR V2X for NR vehicular communications, including support of NR frame structure, PSCCH and PSSCH multiplexing, resource allocation for NR V2X using mode 2 (autonomous resource selection), SCI (Sidelink Control Information) update, compliance with scenarios and channel models based on TR 38.885. Our initial focus is on: frequency range 1 with numerologies 0, 1, and 2, TDD system, out-of-coverage scenarios, mode 2 resource allocation, broadcast communications, omnidirectional transmissions/receptions for sidelink, sensing-based semi-persistent scheduling (for basic service messages), slot-based scheduling, PSCCH/PSSCH time multiplexing, and blind retransmissions.


`Architectural description`_
============================

In our simulator, we provide an end-to-end environment. As an example,
please take a look into the following Figure, that describes all the building
blocks from a remote host to a User Equipment:

  .. figure:: {static}/img/e2e.svg
    :figclass: m-fix-w-512
    :alt: E2E vision


More in detail, at the Network Device level, we have implemented the following
layers:

  .. figure:: {static}/img/netdevice.svg
    :figclass: m-fix-w-512
    :alt: In-depth NR layers

You can find more information about the `MAC features <mac>`_, or the
`PHY features <phy>`_, or the `higher layers <core>`_.
