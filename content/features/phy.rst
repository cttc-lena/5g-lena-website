PHY Features
#############

:summary: High-level overview of the feature set offered by the PHY of 5G-LENA
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: *5G-LENA PHY feature overview*

            Are you wondering what we support at PHY level? Here we include a brief description for each macro block.



`NR frame structure and numerologies`_
======================================

Our implementation currently supports the 5G NR numerologies shown in the following [TS38300]_:

.. _tab-numerologies:

.. table:: Implemented NR numerologies

   ===========   ==================   ==================     ================   ========================   ================
   Numerology    Slots per subframe   Symbol length (μs)     Slot length (ms)   Subcarrier spacing (kHz)   Symbols per slot
   ===========   ==================   ==================     ================   ========================   ================
    0 (LTE)      1                         71.42                1                  15                          14
    1            2                         35.71                0.5                30                          14
    2            4                         17.85                0.25               60                          14
    3            8                         8.92                 0.125              120                         14
    4            16                        4.46                 0.0625             240                         14
   ===========   ==================   ==================     ================   ========================   ================

In order to simplify the modeling, the cyclic prefix is included in the OFDM symbol length. For example, for numerology 0, the OFDM symbol length is 1ms/14=71.42μs, while the real symbol length is 1/SCS=66.67μs and the cyclic prefix is 4.76μs. A slot can contain all downlink, all uplink, or at least one downlink part and uplink part. Data transmission can span multiple slots.

`FDM of numerologies`_
======================
When numerologies are multiplexed in a frequency domain, a.k.a., frequency division multiplexing (FDM) of numerologies, each numerology occupies a part of the whole channel bandwidth, which is referred to in NR as a bandwidth part (BWP) [TS38300]_. This is useful to achieve energy saving purposes, as well as for a gNB to accommodate different services with different latency/throughput requirements, as eMBB and URLLC, within the same channel bandwidth.

The 'NR' module supports orthogonal and static FDM of multiple numerologies. An example is shown below for two numerologies.


.. figure:: {static}/img/fdm.PNG
    :figclass: m-fix-w-256
    :alt: Example of FDM of two numerologies.


`Mini-slot and mixed UL-DL slot format support`_
================================================
According to the NR definition of TTI, one TTI duration corresponds to a number of consecutive OFDM symbols in the time domain in one transmission direction, and different TTI durations can be defined when using different number of OFDM symbols (e.g., corresponding to a mini-slot, one slot or several slots in one transmission direction) [TS38300]_. We define a slot as a period of time containing 14 OFDM symbols (which time length depends on the selected numerology). Each OFDM symbol can contain UL/DL control/data. By default, the first OFDM symbol in a slot is devoted for DL control, the last OFDM symbol to UL control, and the OFDM symbols in between can be dynamically allocated to DL and/or UL data.

The 'NR' module supports a dynamic UL-DL slot format [TS38213]_.


`Propagation models`_
=====================
Our implementation currently supports the following path loss models: Indoor-Hotspot (InH) Office-Open, InH Shopping-Mall, InH Office-Mixed, Rural Macro, Urban Macro, Urban Micro Street-Canyon. Line-of-Sight (LOS) and non-LOS (NLOS) conditions can also be enforced.

Outdoor-to-indoor propagation losses and autocorrelation of shadow fading are also included. As additional modelling components there is spatial consistency and blockage, but not oxygen absorption.


`Channel models`_
=================
Fast fading follows step-by-step the 3GPP channel model as per [TR38900]_.


`Antenna element radiation pattern`_
====================================
Isotropical radiation pattern of the antenna elements, as well as 3GPP models are supported. For the 3GPP element radiation pattern, the vertical and horizontal radiation patterns are obtained as in [TR38802]_ (table A.2.1-8 for UE, and table A.2.1-7 for gNB). 


`Antenna array`_
================
A planar antenna array is used both at gNBs and UEs. Rectangular arrays are supported. Only vertical polarization.


`Beamforming methods (array radiation pattern)`_
================================================
The 'NR' module supports two methods: long-term covariance matrix and beam-search. The former assumes knowledge of the channel matrix to produce the optimal transmit and receive beam.
In the later, a set of predefined beams is tested, and the beam-pair providing a highest average SNR is selected. 

For the beam-search method, our simulator supports abstraction of the beam ID through two angles (azimuth and elevation). A new interface allows you to have the beam ID available at MAC layer for scheduling purposes.


`NR PHY abstraction`_
=====================
The 'NR' module includes a new PHY abstraction model for data that accounts for 
LDPC coding and extends the modulations up to 256-QAM,
as compared to the one in ns-3 LENA that used Turbo Codes (TC) and up to 64-QAM.
The developed model capitalizes on the exponential
effective signal-to-interference-plus-noise ratio (SINR)
mapping (EESM), for which the optimal effective SINR mapping
parameters are derived with a calibration procedure using an
NR-compliant link-level simulator. The NR-compliant link-level simulator has also been used 
to obtain lookup tables of block error rate (BLER) vs SINR for various NR configurations (e.g.,
different MCSs, MCS tables, and resource allocations). The developed PHY abstraction 
model supports HARQ based on Incremental Redundancy and on Chase Combining. 
Both MCS Table1 (up to 64-QAM) and MCS Table2 (up to 256-QAM) in NR are supported [TS38214]_ 
and the transport block segmentation follows [TS38212]_, with LDPC base graph types 1 and 2.






.. [TS38300] 3GPP TS 38.300, "TSG RAN; NR; Overall description; Stage 2", Release 15, v15.3.1, Oct. 2018.
.. [TS38213] 3GPP TS 38.213, "TSG RAN; NR; Physical layer procedures for control", Release 15, v15.3.0, Sept. 2018.
.. [TR38900] 3GPP TR 38.900, "TSG RAN; Study on channel model for frequency spectrum above 6 GHz", Release 14, v14.3.1, July 2017.
.. [TR38802] 3GPP TR 38.802, "TSG RAN; Study on New Radio Access Technology Physical Layer Aspects", Release 14, v14.2.0, Sept. 2017.
.. [TS36212] 3GPP TS 36.212, "TSG RAN; Evolved Universal Terrestrial Radio Access (E-UTRA); Multiplexing and channel coding", Release 8, v15.4.0, Jan. 2019.
.. [TS38212] 3GPP TS 38.212, "TSG RAN; NR; Multiplexing and channel coding", Release 15, v15.4.0, Jan. 2019.
.. [TS38214] 3GPP TS 38.214, "TSG RAN; NR; Physical layer procedures for data", Release 15, v15.3.0, Sept. 2018.
