Core Features
#############

:summary: High-level overview of feature set offered by the core of 5G-LENA

:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: *5G-LENA RLC/PDCP/Core feature overview*

            Work in these segments of the network is still to be done.


The simulator currently reuses features available in LENA ns-3 LTE. For details see:

* RLC https://www.nsnam.org/docs/release/3.29/models/html/lte-design.html#rlc
* PDCP https://www.nsnam.org/docs/release/3.29/models/html/lte-design.html#pdcp
* RRC https://www.nsnam.org/docs/release/3.29/models/html/lte-design.html#rrc
* NAS https://www.nsnam.org/docs/release/3.29/models/html/lte-design.html#nas
* EPC https://www.nsnam.org/docs/release/3.29/models/html/lte-design.html#epc-model
