MAC Features
#############

:summary: High-level overview of the feature set offered by the MAC of 5G-LENA
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: *5G-LENA MAC feature overview*

            High-level overview of the feature set offered by the MAC of 5G-LENA.

`Access scheme`_
================
To give flexibility in implementation choice, NR is deliberately introducing functionality to support analog beamforming in addition to digital precoding/beamforming. At high frequencies, analog beamforming, where the beam is shaped after digital-to-analog conversion, is necessary from an implementation perspective. Analog beamforming (or single-beam capability) results in the constraint that a receive or transmit beam can only be formed in a single direction at any given time instant and requires beam-sweeping where the same signal is repeated in multiple OFDM symbols but in different transmit beams. With beam-sweeping possibility, it is ensured that any signal can be transmitted with a high gain and narrow beamformed transmission to reach the entire intended coverage area.

The 'NR' module supports TDMA and OFDMA with single-beam capability and variable TTI. Examples of the supported allocation schemes for a slot are shown in the next figure, assuming that UE1 is in beam1 while UE2 and UE3 are attached to beam2. 

.. figure:: {static}/img/accessmodes.PNG
                :figclass: m-fix-w-768
                :alt: Supported access modes and allocation schemes for a slot. 

`OFDMA`_
********
The implementation of OFDMA under the single-beam capability constraint for mmWave means that frequency-domain multiplexing of different UEs is allowed among UEs associated to the same beam. 

`TDMA`_
*******
TDMA inherently includes the single-beam capability constraint. A single UE is scheduled per TTI. 

`Scheduler`_
============
We have introduced schedulers for OFDMA and TDMA-based access with variable TTI under single-beam capability. The main output of a scheduler functionality is a list of DCIs for a specific slot, each of which specifies four parameters: the transmission starting symbol, the duration (in number of symbols) and an RBG bitmask, in which a value of 1 in the position x represents a transmission in the RBG number x. The current implementation of schedulers API follows the FemtoForum specification for LTE MAC Scheduler Interface [ff-api]_, but can be easily extended to be compliant with different industrial interfaces.

The NR module currently offers three specializations of the TDMA and OFMA schedulers. These specializations are, performing the downlink scheduling in a round robin (RR), proportional fair (PF) and max rate (MR) manner, as explained in the following:

* RR: the available RBGs are divided evenly among UEs associated to that beam
* PF: the available RBGs are distributed among the UEs according to a PF metric that considers the actual rate (based on the CQI) elevated to α and the average rate that has been provided in the previous slots to the different UEs. Changing the α parameter changes the PF metric. For α=0, the scheduler selects the UE with the lowest average rate. For α=1, the scheduler selects the UE with the largest ratio between actual rate and average rate.
* MR: the total available RBGs are distributed among the UEs according to a maximum rate (MR) metric that considers the actual rate (based on the CQI) of the different UEs.


`Uplink delay support for UL data`_
===================================

In an NR system, the UL decisions for a slot are taken in a different moment than the DL decision for the same slot. In particular, since the UE must have the time to prepare the data to send, the gNB takes the UL scheduler decision in advance and then sends the UL grant taking into account these timings. For example, consider that the DCIs for DL are usually prepared two slots in advance with respect to when the MAC PDU is actually over the air. For example, for UL, the UL grant must be prepared four slots before the actual time in which the UE transmission is over the air transmission: after two slots, the UL grant will be sent to the UE, and after two more slots, the gNB is expected to receive the UL data (which is refered in NR specifications as K2 [TS38213]_, [TS38214]_). Please note that the latter examples consider default values for MAC to PHY processing delays at gNB and UE, which are in NR module set to 2 slots.

To accommodate the NR UL scheduling delay, the new MAC scheduler design is actively considering these delays during each phase.

.. figure:: {static}/img/sched.png
                :figclass: m-fix-w-512
                :alt: Scheduler timings in the ns-3 NR simulator, for K2=2 slots and L1L2 control/data latencies of 2 slots. 
                
`Scheduling requests`_
======================

The NR module correctly models the Scheduling Request (SR), that is a control message that the UE sends to the gNB to request an initial allocation to transmit some data and a Buffer Status Report (BSR), eventually continuing the allocation and hence the data transmission [TS38300]_. In our simulator, the BSR can only be sent in conjunction with a MAC PDU. Note that according to NR specifications, the BSR is part of the MAC header [TS38321]_, so that the BSR cannot be sent periodically and ideally.

.. figure:: {static}/img/ULhandshake_papnat.png
                :figclass: m-fix-w-768
                :alt:  UL handshake procedure for NR UL grant-based access, including NR timings and processing delays, for K2=0 (top) and K2=2 slots (bottom). 
                

.. [ff-api] FemtoForum , "LTE MAC Scheduler Interface v1.11", Document number: FF_Tech_001_v1.11 , Date issued: 12-10-2010.
.. [TS38300] 3GPP TS 38.300, "TSG RAN; NR; Overall description; Stage 2", Release 15, v15.3.1, Oct. 2018.
.. [TS38213] 3GPP TS 38.213, "TSG RAN; NR; Physical layer procedures for control", Release 15, v15.3.0, Sept. 2018.
.. [TS38214] 3GPP TS 38.214, "TSG RAN; NR; Physical layer procedures for data", Release 15, v15.3.0, Sept. 2018.
.. [TS38321] 3GPP TS 38.321, "TSG RAN; NR; Medium Access Control (MAC) protocol specification", Release 15, v15.4.0, Jan. 2019.

