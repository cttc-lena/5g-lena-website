5G-LENA module
##############

:save_as: index.html
:url:
:cover:
:description: C++11 module for the network simulator ns-3 for 3GPP 5G networks simulation
:summary: GPLv2 ns-3 module to simulate 3GPP networks
:hide_navbar_brand: True

:landing:
    .. container:: m-row

        .. container:: m-col-l-10 m-push-l-1 m-col-m-12 m-nopadb

            .. raw:: html

                <h1 style="font-size: xxx-large; color: #403b5f; text-align: center; font-weight: bold;">
                    5G-LENA: The 5G NR module for the ns-3 simulator
                </h1>

    .. container:: m-row

        .. container:: m-col-m-4

          .. block-success:: ns-3 module to simulate 3GPP 5G networks

            5G-LENA is an open-source 5G New Radio (NR) network simulator, designed as a
            pluggable module to `ns-3 <https://www.nsnam.org/>`_. Its development,
            initially funded by InterDigital, is open to the community under
            GPLv2 license in order to foster early adoption, contributions by
            industrial and academic partners, collaborative development and
            results reproducibility.

            The 5G-LENA simulator enables end-to-end simulations from the application
            layer down to the physical layer and thus provides invaluable insights
            into the design of industrial 5G solutions and allows early assessment
            of the potential performance of such solutions.

        .. container:: m-col-m-5

          .. block-info:: Discover 5G-LENA

            .. youtube:: PM1c4Vf_DSA

        .. container:: m-col-m-3

            .. button-primary:: {filename}/download.rst
                :class: m-fullwidth

                Get the software

            .. button-primary:: https://groups.google.com/g/5g-lena-users/ 
                :class: m-fullwidth

                Join 5G-LENA 
                Group

            .. button-primary:: https://5g-lena.cttc.es/contact/
                :class: m-fullwidth

                Contact us

            .. figure:: https://developers.google.com/open-source/gsoc/resources/downloads/GSoC-Horizontal.png
                :target: https://www.nsnam.org/wiki/GSOC2024Projects#5G_NR_models_enhancements



.. role:: raw-html(raw)
    :format: html

.. container:: m-row m-container-inflate

    .. container:: m-col-m-4

        .. figure: : {filename}/img/feature-6.png
            :figclass: m-fullwidth m-warning
            :alt: Core features

        .. block-primary:: Inherit the strengths of *ns-3*

            `ns-3 <https://www.nsnam.org/>`_ is an open source discrete-event
            network simulator/emulator.
            The ns-3 project is committed to build a solid, well documented, easy to use and debug simulation
            core,
            which caters to the needs of the entire simulation workflow,
            from simulation configuration to trace collection and analysis.

            Compared to other open source simulators, ns-3 offers multi-RAT (Radio Access Technology)
            and multi-band simulation capabilities, in which multiple technologies
            can operate and coexist: NR, NR-U, LTE (LTE-A, LAA, LTE-U) Wi-Fi, WiGig, etc.


            .. button-primary:: {filename}/features.rst
                :class: m-fullwidth

                See the 5G-LENA features

    .. container:: m-col-m-4

        .. figure: : {filename}/img/feature-9.png
            :figclass: m-fullwidth m-info
            :alt: Feature

        .. block-info:: Born in academia

            5G-LENA is being designed, developed and maintained by the
            `Open Simulations research unit (OpenSim) <https://www.cttc.cat/open-simulations-opensim/>`_
            of `CTTC <https://www.cttc.es>`_ (Centre Tecnològic de
            Telecomunicacions de Catalunya). OpenSim research group 
            maintains a strong commitment with the ns-3 community in 
            the areas of NR, NR V2X, NR-U, O-RAN, LTE, and
            its evolutions in licensed and unlicensed spectrum.

            The extensive study and calibration of the 5G-LENA models, 
            showcased in numerous peer-reviewed publications, 
            establishes it as a reference 5G NR simulator for conducting comprehensive
            and in-depth research.

            .. button-info:: {filename}/papers.rst
                :class: m-fullwidth

                See our publications

    .. container:: m-col-m-4

        .. figure: : {filename}/img/feature-7.png
            :figclass: m-fullwidth m-success
            :alt: Feature

        .. block-success:: ... and for industries?

            We have successfully collaborated with many industries and agencies like
            `Ubiquisys <http://www.cttc.es/project/lte-epc-network-simulator/>`_,
            `WiFi Alliance (WFA) <http://www.cttc.es/project/wfa-licensed-assisted-access-2/>`_,
            `Spidercloud Wireless <http://www.cttc.es/project/spidercloud-licensed-assisted-access/>`_,
            `InterDigital <http://www.cttc.es/project/industrial-project-funded-by-interdigital/>`_,
            `National Institute of Standards and Technologies (NIST) <http://www.cttc.es/project/modeling-simulation-and-performance-evaluation-for-future-public-safety-networks/>`_,
            Lawrence Livermore National Lab (LLNL), Meta (Facebook), Optare, Google Summer of Code and many
            EU projects funded in the framework of FP7, H2020, and HORIZON-JU-SNS.

            For any inquiry, contact the CTTC's `OpenSim <mailto:info-5glena@llistes.cttc.es>`_
            team for research, development and innovation projects using 5G-LENA.
            Our team has a strong expertise on modeling, design, development, testing,
            3GPP standards and their practical implementation. Why don’t you consider us for
            your next R&D&I proposal?

            .. button-success:: {filename}/contact.rst
                :class: m-fullwidth

                Explore collaboration possibilities
