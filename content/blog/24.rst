5G-LENA v3.0 is available
#########################

:summary: New release available from February 2024 (v3.0)
:date: 02-16-2024
:authors: CTTC
:categories: general
:tags: information, software

5G-LENA v3.0 (compatible with ns-3.41)
======================================

Hi all,

We are pleased to announce that 5G-LENA v3.0 is now available for download. This release is compatible with the latest ns-3.41. 

One of the most important additions to this release is the support of a completely new full MIMO model. Comparing to the previous dual-polarized implementation, this new model offers a much more general approach and can be easily extended to support different types of MIMO, which was not the case with the previous implementation which was limited to a particular case of MIMO, i.e., DualPolarized-MIMO (DP-MIMO), supporting only 2 streams with no possibility for extension. Old DP-MIMO implementation used some assumptions related to the mapping of the streams to ports that were limiting its application, i.e., each stream was mapped to the antenna elements with a specific polarization. This means that there was a hard limitation on the assignment of streams to a specific port and to the specific antenna elements of one polarization. Also, it used a not realistic inter-stream interference rejection model. Instead, the new MIMO implementation supports general MIMO, through the inclusion of 3GPP-compliant PMI (precoding matrix indicator) and RI (rank indicator), assuming MMSE-IRC (interference rejection combining) receiver for inter-stream interference suppression, as adopted in 3GPP. Such general MIMO includes as particular case the old DP-MIMO, but with a more generic and realistic model for DP-MIMO in which the streams/port/antennas mapping is not predefined but defined through digital precoding and in which a more accurate model for the inter-stream interference calculation is considered. The new MIMO is flexible and can be easily extended for more streams/ranks/ports. 

Let us note that the current MIMO implementation requires Eigen3 library (https://eigen.tuxfamily.org/). 

The cttc-nr-mimo-demo example shows how to use and configure the new MIMO model. See the NR manual and Doxygen for the full description of the new MIMO model and APIs. 
The new APIs are also listed CHANGES.md. 

When creating an MR, 5G-LENA users will be able to use CI/CD minutes belonging to the NR module project. 

The code base is updated and a bit modernized, e.g. to use standard integer types, simpler bool checks, spelling errors are fixed, automatic clang-tidy fixes are applied, for loops are modernized, python formatter settings were added, etc. 

In addition to all this, the NR module pipelines have been improved in multiple aspects. You can read a detailed description in the CHANGES.md and the RELEASE_NOTES.md files. 

We would like to thank all 5G-LENA users who contribute continuously to the 5G-LENA community by opening issues, proposing solutions, and answering on 5G-LENA users list. Thank you for your support! 

To cite this NR 3.0 5G-LENA release please use: https://zenodo.org/records/10670856 (in addition to the reference papers listed in https://5g-lena.cttc.es/papers/) 

Let us finally remark that, a part from the Blog in https://5g-lena.cttc.es/, we have created a new group on LinkedIn, where you can follow our latest updates: https://www.linkedin.com/company/101470387/admin/feed/posts/ 

The 5G-LENA team
