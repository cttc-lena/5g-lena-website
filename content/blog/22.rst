5G-LENA v2.6 is available
#########################

:summary: New release available from December 2023 (v2.6)
:date: 12-01-2023
:authors: CTTC
:categories: general
:tags: information, software


5G-LENA v2.6 (compatible with ns-3.40)
======================================

We are pleased to announce that 5G-LENA v2.6 is now available for download. This release is compatible with the latest ns-3.40.

One of the most important additions to this release is the NR tutorial created by Giovanni Grieco in the scope of the GSoC 2023 project "IUNS-3 5G NR: Improving the Usability of ns-3's 5G NR Module" mentored by Tom Henderson, Katerina Koutlia, and Biljana Bojovic. The main objective of this tutorial was to ease the learning curve for new users of the NR module. The tutorial is already available at the 5G-LENA website: https://5g-lena.cttc.es/.

We are sorry for not being very active for several months on the user's list and in Gitlab issues. This is because most of the team was off for many months. We are now again back on track, and with some new members,  so we hope to be able to answer all your questions and resolve the open issues.

We would like to thank our team member Katerina Koutlia for her huge effort during all these months while the other team members were away. Thanks also to Giovanni Grieco and Tom Henderson for their contributions to 5G-LENA through the NR Tutorial GSoC project https://www.nsnam.org/wiki/GSOC20235GUsability. Also, big thanks to all 5G-LENA users who contribute continuously to the 5G-LENA community by opening issues, proposing solutions, and answering on 5G-LENA users list. Thank you for your support!

To cite this NR 2.6 5G-LENA release please use: https://zenodo.org/records/10246105

The 5G-LENA team
