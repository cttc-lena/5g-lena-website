An E2E simulator for 5G NR networks
###################################

:summary: Main journal paper accepted for Elsevier SIMPAT
:date: 25-05-2019
:authors: CTTC
:categories: general
:tags: papers

`The 5G-LENA main journal paper has been accepted`_
===================================================

Hello everyone, we are pleased to inform you that our initial submission
for 5G-LENA has been accepted for publication by the prestigious journal 
*Simulation Modelling Practice and Theory*, published by **Elsevier**.
You can access the paper from `this link <https://www.sciencedirect.com/science/article/pii/S1569190X19300589>`_.
Please remember to cite it in your publications, if your work is based on ours::

  @article{5GLENA,
  title = {{A}n {E2E} simulator for {5G} {NR} networks},
  journal = "Simulation Modelling Practice and Theory",
  volume = "96",
  pages = "101933",
  year = "2019",
  issn = "1569-190X",
  doi = "https://doi.org/10.1016/j.simpat.2019.101933",
  url = "http://www.sciencedirect.com/science/article/pii/S1569190X19300589",
  author = "Natale Patriciello and Sandra Lagen and Biljana Bojovic and Lorenza Giupponi",
  keywords = "ns-3, NR, Network simulator, E2E Evaluation, Calibration",
  abstract = "As the specification of the new 5G NR standard proceeds inside 3GPP, the availability of a versatile, full-stack, End-To-End (E2E), and open source simulator becomes a necessity to extract insights from the recently approved 3GPP specifications. This paper presents an extension to ns-3, a well-known discrete-event network simulator, to support the NR Radio Access Network. The present work describes the design and implementation choices at the MAC and PHY layers, and it discusses a technical solution for managing different bandwidth parts. Finally, we present calibration results, according to 3GPP procedures, and we show how to get E2E performance indicators in a realistic deployment scenario, with special emphasis on the E2E latency."
  }

We would like to use this occasion to thank the many of you which have
requested the access to the software, and are daily working to improve it.

The 5G-LENA Team
