WNS3 papers
###########

:summary: WNS3: NR radio environmental maps, NR V2X channel models, realistic beamforming and SRS
:date: 06-12-2021
:authors: CTTC
:categories: general
:tags: papers

`Three papers in the Workshop on NS3`_
======================================

We are pleased to inform you that we got three papers accepted for publication and presentation in the Workshop on ns-3, taking place June 21-24 (virtual event). The details of the papers are as follows:

* *K. Koutlia, B. Bojovic, S. Lagen, L. Giupponi,*
  **Novel Radio Environment Map for the ns-3 NR Simulator**, 
  in Workshop on ns-3, June 2021. Available `here <https://dl.acm.org/doi/abs/10.1145/3460797.3460803>`__. 

* *T. Zugno, M. Drago, S. Lagen, Z. Ali, M. Zorzi*, 
  **Extending the ns-3 Spatial Channel Model for Vehicular Scenarios**, 
  in Workshop on ns-3, June 2021. Available `here <https://dl.acm.org/doi/10.1145/3460797.3460801>`__.

* *B. Bojovic, S. Lagen, L. Giupponi*, 
  **Realistic beamforming design using SRS-based channel estimate for ns-3 5G-LENA module**, 
  in Workshop on ns-3, June 2021. Available `here <https://dl.acm.org/doi/abs/10.1145/3460797.3460809>`__.

We would like to use this occasion to thank all of you which have
requested the access to the software, and are daily working to improve it through issues reports, feedback, and patches proposals.

The 5G-LENA Team
