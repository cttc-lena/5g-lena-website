5G-LENA v2.5 is available
#########################

:summary: New release available from July 2023 (v2.5)
:date: 07-27-2023
:authors: CTTC
:categories: general
:tags: information, software


5G-LENA v2.5 (compatible with ns-3.39)
======================================

We are pleased to announce that 5G-LENA v2.5 is now available for download. This release is compatible with the latest ns-3.39.

Some of the most important features of the 5G-LENA v2.5 include new QoS MAC schedulers, new design for the Logical Channel bytes assignment, including a new algorithm based on the QoS requirements of the flows, support of Release 18 5QIs and support of delay-critical GBR resource type.

All the included features can be found in RELEASE_NOTES.md and CHANGES.md.

The information about the recommended ns-3 releases and how to install nr can be found in the README.md file or https://cttc-lena.gitlab.io/nr/html/index.html#getting-started, while all the available features are listed in CHANGES.md and RELEASE_NOTES.md.

Updating the software is as easy as doing a git pull command, for both ns-3-dev and nr module. For ns-3-dev, make sure you go to the ns-3.39 branch, and for nr on 5g-lena-v2.5.y branch. For any problem related to the installation, please open an issue ticket on the NR gitlab repository. For any comment, doubt, and discussion, remember that we have a google group for the 5G-LENA users: https://groups.google.com/g/5g-lena-users.

We would love to hear from you if you have any feedback, possible code contributions, or an idea for possible collaboration to improve 5G-LENA!

Thank you for your support!

The 5G-LENA team
