Papers
######

:summary: List of paper based on, or using, 5G-LENA
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: 5G-LENA academic publications


`To cite 5G-LENA in general:`_
==============================

* *N. Patriciello, S. Lagen, B. Bojovic, L. Giupponi*, **An E2E Simulator for 5G NR Networks**, Elsevier Simulation Modelling Practice and Theory (SIMPAT), vol. 96, 101933, Nov. 2019. 

* *K. Koutlia, B. Bojovic, Z. Ali, S. Lagen*, **Calibration of the 5G-LENA System Level Simulator in 3GPP reference scenarios**, Elsevier Simulation Modelling Practice and Theory (SIMPAT), vol. 119, 102580, Sept. 2022. 

* *B. Bojovic, S. Lagen*, **3GPP-Compliant Single-User MIMO Model for High-Fidelity Mobile Network Simulations**, Computer Networks, Volume 256, Jan. 2025.

`For the NR extensions, please do consider:`_
============================================= 

* NR-U: N. Patriciello, S. Lagen, B. Bojovic, L. Giupponi, **NR-U and IEEE 802.11 Technologies Coexistence in Unlicensed mmWave Spectrum: Models and Evaluation**, IEEE Access, vol. 8, pp. 71254-71271, 2020.

* NR V2X: Z. Ali, S. Lagen, L. Giupponi, R. Rouil, **3GPP NR V2X Mode 2: Overview, Models and System-level Evaluation**, IEEE Access, vol. 9, pp. 89554-89579, June 2021.

`Journal papers`_
=================

* *B. Bojovic, S. Lagen,*
  **3GPP-Compliant Single-User MIMO Model for High-Fidelity Mobile Network Simulations**, 
  Elsevier Computer Networks, Volume 256, Jan. 2025.

* *K. Koutlia, S. Lagen,*
  **On the impact of Open RAN Fronthaul Control in scenarios with XR Traffic**, 
  Elsevier Computer Networks, Volume 253, 2024.

* *J. Viana, H. Farkhari, L. Campos, K. Koutlia, B. Bojovic, S. Lagen, R. Dinis,*
  **Deep Attention Recognition for Attack Identification in 5G UAV Scenarios: Novel Architecture and End-to-End Evaluation**,
  IEEE Trans. on Vehicular Technology, vol. 73, no. 1, pp. 131-146, Jan. 2024.
  
* *L. Vignati, G. Nardini, M. Centenaro, P. Casari, S. Lagen, B. Bojovic, S. Zambon, L. Turchet,*
  **Is Music in the Air? Evaluating 4G and 5G Support for the Internet of Musical Things**, 
  IEEE Access, 2024.

* *A. Larrañaga, M. Carmen Lucas-Estan, S. Lagen, Z. Ali, I Martinez, J. Gozalvez,* 
  **An  open-source implementation and validation of 5G NR Configured Grant for URLLC in ns-3 5G LENA: a scheduling case study in Industry 4.0 scenarios**, 
  Elsevier Journal of Network and Computer Applications, vol. 215,  2023.

* *B. Bojovic, S. Lagen, K. Koutlia, X. Zhang, P. Wang, L. Yu,*
  **Enhancing 5G QoS Management for XR Traffic Through XR Loopback Mechanism**, 
  IEEE Journal on Selected Areas in Communications, vol. 41, no. 6, pp. 1772-1786, June 2023.

* *K. Koutlia, B. Bojovic, S. Lagen, X. Zhang, P. Wang, J. Liu,*
  **System Analysis of QoS Schedulers for XR Traffic in 5G NR**, 
  Elsevier Simulation Modelling Practice and Theory 125 (2023), 102745. 

* *S. Lagen, B. Bojovic, K. Koutlia, X. Zhang, P. Wang, Q. Qu,*
  **QoS Management for XR Traffic in 5G NR: A Multi-Layer System View & End-to-End Evaluation**, 
  IEEE Communications Magazine, May 2023.

* *S. Lagen, X. Gelabert, L. Giupponi, A. Hansson*, 
  **Fronthaul-aware Scheduling Strategies for Dynamic Modulation Compression in Next Generation RANs**, 
  IEEE Transactions on Mobile Computing, vol. 22, no. 5, pp. 2725-2740, May 2023, `DOI <https://doi.org/10.1109/TMC.2021.3128700>`__.

* *K. Koutlia, B. Bojovic, Z. Ali, S. Lagen,*
  **Calibration of the 5G-LENA System Level Simulator in 3GPP reference scenarios**, 
  Elsevier Simulation Modelling 
  Practice and Theory (SIMPAT), May 2022. 
  Available `here <https://arxiv.org/abs/2205.03278>`__.

* *S. Lagen, X. Gelabert, A. Hansson, M. Requena, L. Giupponi*, 
  **Fronthaul Compression Control for shared Fronthaul Access Networks**, 
  IEEE Communications Magazine, May 2022. Available `here <https://arxiv.org/abs/2205.08329>`__.

* *Z. Ali, S. Lagen, L. Giupponi, R. Rouil,*
  **3GPP NR V2X Mode 2: Overview, Models and System-level Evaluation**, 
  IEEE Access, vol. 9, pp. 89554-89579, June 2021, `DOI <https://doi.org/10.1109/ACCESS.2021.3090855>`__.

* *S. Lagen, L. Giupponi, A. Hansson, X. Gelabert,*
  **Modulation Compression in Next Generation RAN: Air Interface and Fronthaul trade-offs**, 
  IEEE Communications Magazine, Vol. 59, No. 1, pp. 89-95, January 2021, `DOI <https://doi.org/10.1109/MCOM.001.2000453>`__. 

* *N. Patriciello, S. Lagen, B. Bojovic, L. Giupponi,* 
  **NR-U and IEEE 802.11 Technologies Coexistence in Unlicensed mmWave Spectrum: Models and Evaluation**,
  IEEE Access, vol. 8, pp. 71254-71271, 2020, 
  `DOI <https://doi.org/10.1109/ACCESS.2020.2987467>`__.

* *S. Lagen, L. Giupponi, S. Goyal, N. Patriciello, B. Bojovic, A. Demir, M. Beluri,*
  **New Radio Beam-based Access to Unlicensed Spectrum: Design Challenges and Solutions**,
  IEEE Communications Surveys & Tutorials, Oct. 2019, 
  `DOI <https://doi.org/10.1109/COMST.2019.2949145>`__. 
  Available `here <{static}/archive/NRU_COMST.pdf>`__.
  
* *N. Patriciello, S. Lagen, B. Bojovic, L. Giupponi,*
  **An E2E Simulator for 5G NR Networks**, 
  Elsevier Simulation Modelling 
  Practice and Theory (SIMPAT), vol. 96, 101933, Nov. 2019, 
  `DOI <https://doi.org/10.1016/j.simpat.2019.101933>`__. 
  Available `here <http://arxiv.org/abs/1911.05534>`__.
  


`Conference papers`_
====================

* *F. Rezazadeh, A. Ashtari, S. Lagen, J. Mangues, D. Niyato, L. Liu,*
  **GenOnet: Generative Open xG Network Simulation with Multi-Agent LLM and ns-3**, 
  in 2024 3rd Int. Conf. on 6G Networking (6GNet), Paris (France), Oct. 2024.

* *V. Timcenko, S. B. Rakas, S. Lagen, B. Bojovic, K. Koutlia and C. A. Haro,*
  **Attack Identification and Classification in V2X Scenarios**,
  in 2024 23rd Int. Symp. INFOTEH-JAHORINA (INFOTEH), East Sarajevo (Bosnia and Herzegovina), 2024.

* *G. Frangulea, B. Bojovic, S. Lagen,* 
  **NR-U and WiFi Coexistence in sub-7 GHz bands: Implementation and Evaluation of NR-U Type 1 Channel Access in NS3**, 
  in Workshop on ns-3, June 2024.

* *N. Villegas, A. Larrañaga, L. Diez, K. Koutlia, S. Lagen, R. Agüero,*
  **Extending QoS-aware scheduling in ns-3 5G-LENA: A Lyapunov based solution**, 
  in Workshop on ns-3, June 2024.

* *K. Koutlia, S. Lagen, B. Bojovic,* 
  **Enabling QoS Provisioning Support for Delay-Critical Traffic and Multi-Flow Handling in ns-3 5G-LENA**, 
  in Workshop on ns-3, June 2023. 

* *M. Pagin, S. Lagen, B. Bojovic, M. Polese, M. Zorzi,*
  **Improving the Efficiency of MIMO Simulations in ns-3**,
  in Workshop on ns-3, June 2023. 

* *B. Bojovic, S. Lagen*, 
  **Enabling NGMN mixed traffic models for ns-3**, 
  in Workshop on ns-3, June 2022. 

* *B. Bojovic, Z. Ali, S. Lagen*, 
  **ns-3 and 5G-LENA Extensions to Support Dual-Polarized MIMO**, 
  in Workshop on ns-3, June 2022.
  
* *S. Lagen, X. Gelabert, L. Giupponi, A. Hansson*, 
  **Fronthaul-Aware Scheduling Strategies for Next Generation RANs**, 
  in Proceedings of Global Communications Conference (GLOBECOM), 7-11 December 2021, Madrid (Spain). 

* *Z. Ali, S. Lagen, L. Giupponi*, 
  **On the Impact of Numerology in NR V2X Mode 2 with Sensing and Random Resource Selection**, 
  in Proceedings of IEEE Vehicular Networking Conference (VNC), Nov. 2021.

* *S. Lagen, X. Gelabert, L. Giupponi, A. Hansson*, 
  **Semi-Static Modulation Compression Optimization for Next Generation RANs**, 
  in Proceedings of IEEE International Conference on Communications (ICC), 14-23 June 2021. Available `here <https://zenodo.org/record/4551723#.YUR3jX3tY2w>`__.

* *B. Bojovic, S. Lagen, L. Giupponi*, 
  **Realistic beamforming design using SRS-based channel estimate for ns-3 5G-LENA module**, 
  in Workshop on ns-3, June 2021. Available `here <https://dl.acm.org/doi/abs/10.1145/3460797.3460809>`__.

* *T. Zugno, M. Drago, S. Lagen, Z. Ali, M. Zorzi*, 
  **Extending the ns-3 Spatial Channel Model for Vehicular Scenarios**, 
  in Workshop on ns-3, June 2021. Available `here <https://dl.acm.org/doi/10.1145/3460797.3460801>`__.

* *K. Koutlia, B. Bojovic, S. Lagen, L. Giupponi,*
  **Novel Radio Environment Map for the ns-3 NR Simulator**, 
  in Workshop on ns-3, June 2021. Available `here <https://dl.acm.org/doi/abs/10.1145/3460797.3460803>`__. 

* *T. Zugno, M. Polese, N. Patriciello, B. Bojovic, S. Lagen, M. Zorzi,*
  **Implementation of A Spatial Channel Model for ns-3**,
  in Workshop on ns-3, June 2020. 
  Available `here <https://arxiv.org/abs/2002.09341>`__.
  
* *S. Lagen, N. Patriciello, L. Giupponi,*
  **Cellular and Wi-Fi in Unlicensed Spectrum: Competition leading to Convergence**, 
  in 6G Wireless Summit, 17-20 March 2020, Levi (Finland). Available `here <https://zenodo.org/record/4428381#.YMTOB0ztY2w>`__.
  
* *S. Lagen, K. Wanuga, H. Elkotby, S. Goyal, N. Patriciello, L. Giupponi,*
  **New Radio Physical Layer Abstraction for System-Level Simulations of 5G Networks**, 
  in Proceedings of IEEE International Conference on Communications (IEEE ICC), 7-11 June 2020, Dublin (Ireland). 
  Available `here <http://arxiv.org/abs/2001.10309>`__.
  
* *N. Patriciello, S. Lagen, L. Giupponi, B. Bojovic,*
  **The impact of NR Scheduling Timings on End-to-End Delay for Uplink Traffic**,
  in Proceedings of IEEE Global Communications Conference (IEEE GC), 9-14 December 2019, 
  Waikoloa (HI, USA). Available `here <{static}/archive/K2_GC.pdf>`__.

* *N. Patriciello, S. Lagen, L. Giupponi, B. Bojovic,*
  **An Improved MAC Layer for the 5G NR ns-3 module**,
  in Workshop on ns-3, 19-20 June 2019, Florence (Italy). 
  Available `here <https://zenodo.org/record/3359064#.Xcvd2NV7m70>`__.
  
* *N. Patriciello, S. Lagen, L. Giupponi, B. Bojovic,*
  **5G New Radio Numerologies and their Impact on the End-To-End Latency**,
  in Proceedings of IEEE International Workshop on Computer-Aided Modeling Analysis 
  and Design of Communication Links and Networks (IEEE CAMAD), 17-19 September 2018, Barcelona (Spain),
  `DOI <https://doi.org/10.1109/CAMAD.2018.8514979>`__. 
  Available `here <https://zenodo.org/record/2525744#.XcvdwdV7m70>`__.

* *S. Lagen, B. Bojovic, S. Goyal, L. Giupponi, J. Mangues,*
  **Subband Configuration Optimization for Multiplexing of Numerologies in 5G TDD New Radio**,
  in Proceedings of IEEE International Symposium on Personal, Indoor and 
  Mobile Radio Communications (IEEE PIMRC), 9-12 September 2018, Bolonga (Italy), 
  `DOI <https://doi.org/10.1109/PIMRC.2018.8580813>`__. 
  Available `here <https://zenodo.org/record/2574937#.XcvdSdV7m72>`__.
  
* *B. Bojovic, S. Lagen, L. Giupponi,*
  **Implementation and Evaluation of Frequency Division Multiplexing of 
  Numerologies for 5G New Radio in ns-3**,
  in Workshop on ns-3, 13-14 June 2018, Surathkal (India). 
  Available `here <https://zenodo.org/record/1451528#.XcvdotV7m70>`__.

