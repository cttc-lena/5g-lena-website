About
######

:summary: About CTTC
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: *Behind 5G-LENA, you will always find professionals.*

            We are a public founded research center located
            near Barcelona, Spain. Our history is our
            business card, and before putting your trust
            in us, read our history.

About OpenSim research unit and its contribution to ns-3 project
================================================================

The `Open Simulations research unit <https://www.cttc.cat/open-simulations-opensim/>`_ focuses on developing models, algorithms, and architectures for next-generation virtualized open radio access networks. This includes designing, implementing, validating, and evaluating 5G and beyond extensions in ns-3 (including licensed/unlicensed/shared-based access and vehicular communications), as well as deriving 3GPP/IEEE technologies coexistence and spectrum sharing strategies. At OpenSim, we also focus on the design of radio/spectrum/interference/QoS/traffic management techniques.

The OpenSim RU has the vision of collaborative research through open-source code developments. In particular, we believe that technology development and standardization should be based on common software platforms and joint efforts, from which the research and industrial community would benefit and progress together if properly managed. 
For these reasons, we base our research on ns-3, an open-source end-to-end network simulator, and release major part of our developments and findings. Our involvement with ns-3, and particularly 5G-LENA, is our major singularity. Also, we have a strong commitment with the ns-3 community. 

5G-LENA is a GPLv2 NR network simulator, designed as a pluggable module to ns-3. Its development is open to the community in order to foster early adoption, contributions by industrial and academic partners, collaborative development, and results reproducibility. Most of the current version of LENA was developed between 2011 and 2013 as part of an industrial project funded by Ubiquisys Ltd. (now part of Cisco). The work around the NR protocol stack in 5G-LENA was initiated in 2017 by OpenSim members, in the framework of a collaboration with Interdigital. The simulators have received funding from different companies and governmental agencies like the WiFi Alliance, Spidercloud Wireless, NIST, LLNL, Huawei, Google, and Meta. Branches for LTE-LAA, LTE-U, D2D, NR V2X and NR-U, are also available.

Currently we have open projects and developments in the areas of:

* NR,
* NR in unlicensed bands (NR-U), 
* NR Vehicle to Everything communications (NR V2X),
* NR for drones,
* NR for Industry 4.0 scenarios,
* NR for extended reality applications

Interested, drop us an email!


About CTTC
==========

The `Centre Tecnològic de Telecomunicacions de Catalunya (CTTC) <https://www.cttc.es>`_ is a public, non-profit 
research institution based in Castelldefels (Barcelona), resulting from a public initiative of the Regional 
Government of Catalonia (Generalitat de Catalunya). Research activities, both fundamental and applied are organized 
onto four research divisions: Communication Networks, Communication Systems, Communication Technologies and Geomatics.
Since its creation in 2001, CTTC has participated in more than 200 projects funded by European, Spanish or Catalan 
administrations or by direct contracts with industry, in the fields of, among others, 4/5G broadband communication 
systems, embedded systems, next-generation all-optical transport networks, software defined networking, satellite
communications, positioning systems, M2M communications, smart grids. CTTC also develops strategic internal research 
projects for proof of concept and experimental prototypes.

In terms of scientific production, the CTTC annually produces over 60 and 150 papers in peer-reviewed journals and
international conferences, respectively. Currently more than 100 people work at CTTC. In addition, CTTC hosts a 
pre-doctoral research program that since its creation has provided scientific guidance to more than 50 students, 
and produced more than 40 PhD thesis, many of them awarded with prizes from academic institutions or technical societies. CTTC also manages an IPR portfolio of 67 granted patents, belonging to 20 families, awarded in different countries in EU, USA, Japan, Hong Kong, and China.
