Get the software
################

:summary: Get the software
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: *5G-LENA is available for download*
            
            When obtaining this code, you should realize that there is a lot of work and 
            goodwill behind it.
                        
            We appreciate any feedback, bug reports, bug fixes, new features, 
            and community engagement (e.g., `5G-LENA users group <https://groups.google.com/g/5g-lena-users>`_). 
            
            For citing this work, refer to our `Publications section <https://5g-lena.cttc.es/papers/>`_.
            
           
`We offer three simulators:`_
=============================

+ NR module (aka 5G-LENA), released as a standalone ns-3 module
+ NR V2X extension, released as a branch of 'nr' with a patched ns-3
+ NR-U module, released as an additional module to the nr module

`Getting the NR module`_
========================

The NR module is openly available under the terms of GPLv2 license and can be downloaded from here: `https://gitlab.com/cttc-lena/nr <https://gitlab.com/cttc-lena/nr>`_.

The instructions on how to download the NR module are available in `NR README.md <https://cttc-lena.gitlab.io/nr/html/md__builds_cttc_lena_nr__r_e_a_d_m_e.html>`_.

In the following video we show how to properly install the software step-by-step:

.. youtube:: 3tDyylZsPy4
    :width: 700
    :height: 394



`NR V2X extension`_
===================
The implementation of NR V2X is divided between a customized ns-3 LTE (RLC and above) and 5G-LENA NR (MAC and PHY) modules. Therefore, it is very important to follow the instruction in `NR V2X README.md <https://gitlab.com/cttc-lena/nr/-/blob/nr-v2x-dev/README.md>`_.

NR V2X documentation can be found `here <https://5g-lena.cttc.es/static/archive/NR_V2X_V0.1_doc.pdf>`_.


`NR-U module`_
==============

The NR-U module is openly available here: `https://gitlab.com/cttc-lena/nr-u <https://gitlab.com/cttc-lena/nr-u>`_. 
Please note that, to use it, you need also to install the NR module. To do so, follow the instruction in `NR-U README.md <https://gitlab.com/cttc-lena/nr-u/-/blob/master/README.md>`_.

Doxygen documentation for the NR-U module can be found here: `https://cttc-lena.gitlab.io/nr-u/ <https://cttc-lena.gitlab.io/nr-u/>`_.


`We welcome contributors!`_
===========================

As you may know, to fund, design, develop and then maintain an open source software for a novel communication technology is a challenging and time expensive task. For this, we would like to foster collaboration with you:

+ If you identify a bug, please let us know through the `Gitlab issue page <https://gitlab.com/cttc-lena/nr/issues>`_;
+ If you have a development plan that you can share, please get in touch with us. We may be able to provide useful suggestion with your design and then maybe your contribution can be more integrated more efficiently and be useful to let the project grow;
+ If you plan to share your code, as the GPLv2 permits, please help us to integrate it so that the work you have done does not become outdated and then impossible to merge;
+ **The more we are, the better we can do**!
