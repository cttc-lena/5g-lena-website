Contact us!
###########

:summary: Contact
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: Interested in collaborating with us?
          
            Our CTTC OpenSim research group has strong expertise in 5G technologies. Our researchers combine an excellent background in telecommunications with solid simulator design and development skills.
            
            Reach us through `email <mailto:info-5glena@llistes.cttc.es>`_,
            `phone <https://www.cttc.cat/contact/>`_, or meet us at `CTTC <https://www.cttc.cat/contact/>`_!

            

Collaboration possibilities with CTTC OpenSim
=============================================

.. container:: m-row
    
    .. container:: m-col-m-3

        .. block-info:: Research and innovation projects

             We focus on innovative 5G and 6G research projects using 5G-LENA. We study, propose, design and develop new features, algorithms, prototypes and innovative solutions.

    .. container:: m-col-m-3

        .. block-info:: Performance evaluation

             We carry out performance evaluations, analysis, calibrations and validation studies using 5G-LENA.

    .. container:: m-col-m-3

        .. block-info:: Consultancy

             We can offer expert knowledge related to 5G-LENA design and development. We can offer a feasibility study for project developments using 5G-LENA.

    
    .. container:: m-col-m-3

        .. block-info:: 5G-LENA training

             Depending on our team's availability, we might be able to offer basic and advanced courses, addressed to both users and developers, to master the knowledge of 5G-LENA modules, ns-3, Git, and LENA.



Interested students
===================

If you are interested in pursuing a PhD or doing a research stay with us, write us 
an `email <mailto:info-5glena@llistes.cttc.es>`_, with subject Interested PhD,
attaching your curriculum and expressing your interest, and we will explore
the different ways to help you organize your stay in Barcelona.

5G-LENA users mailing list
==========================

We have a Google Group for the 5G-LENA users! Feel free to join,
discuss and collaborate with other researchers using 5G-LENA
through the following link: https://groups.google.com/g/5g-lena-users/.
Don't let your research go un-noticed!
