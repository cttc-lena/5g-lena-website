Team
####

:summary: The team behind 5G-LENA and external contributors
:landing:
    .. container:: m-row

        .. container:: m-col-l-12 m-col-m-12 m-nopadb

          .. block-success:: *5G-LENA Team*

            Wonder who is behind 5G-LENA? The developers are listed here.
         
          .. block-success:: *5G-LENA Contributors*

            Also, we are very glad to have external users and contributors. Special thanks are for the contributors (external to CTTC) that have prepared patches or merge requests, contributing to the development of 5G-LENA. The external contributors are listed below, based on the module to which they contributed with their work.


Team members
============

.. container:: m-row m-container-inflate

    .. container:: m-col-m-4

        .. block-info:: `Biljana Bojovic <http://www.cttc.es/people/biljana-bojovic/>`_

            .. figure:: {static}/img/biljana.jpg
                :figclass: m-portrait
                :alt: Biljana photo

    .. container:: m-col-m-4

        .. block-info:: `Katerina Koutlia <https://www.cttc.cat/people/katerina-koutlia/>`_

            .. figure:: https://www.cttc.cat/wp-content/uploads/2022/06/PEOPLE_Katerina-Koutlia.jpg
                :figclass: m-portrait
                :alt: Katerina photo  

    .. container:: m-col-m-4

        .. block-info:: `Sandra Lagén <https://www.cttc.cat/people/sandra-lagen/>`_

            .. figure:: https://www.cttc.cat/wp-content/uploads/2022/06/PEOPLE_Sandra-Lagen-Morancho.jpg
                :figclass: m-portrait
                :alt: Sandra photo
              
    
.. container:: m-row m-container-inflate

    .. container:: m-col-m-4

        .. block-info:: `Ana Larrañaga <http://www.cttc.es/people/ana-larranaga/>`_

            .. figure:: {static}/img/foto_ana.jpg
                :figclass: m-portrait
                :alt: Ana photo

    .. container:: m-col-m-4

        .. block-info:: `Amir Ashtari <http://www.cttc.es/people/amir-ashtari/>`_

            .. figure:: {static}/img/amir_photo.jpg
                :figclass: m-portrait
                :alt: Amir photo

    .. container:: m-col-m-4

        .. block-info:: `Gabriel De Carvalho <https://www.cttc.cat/people/gabriel-de-carvalho/>`_
            
            .. figure:: https://www.cttc.cat/wp-content/uploads/2023/12/Gabriel-Ferreira.png
                :figclass: m-portrait
                :alt: Gabriel photo


Former team members
===================

.. container:: m-row m-container-inflate

    .. container:: m-col-m-4

        .. block-info:: Lorenza Giupponi

            .. figure:: {static}/img/lorenza.jpg
                :figclass: m-portrait
                :alt: Lorenza photo

    .. container:: m-col-m-4

        .. block-info:: Natale Patriciello

            .. figure:: {static}/img/natale.jpg
                :figclass: m-portrait
                :alt: Natale photo

.. container:: m-row m-container-inflate

    .. container:: m-col-m-4

        .. block-info:: Zoraze Ali

            .. figure:: {static}/img/zoraze.jpg
                :figclass: m-portrait
                :alt: Zoraza photo   

    .. container:: m-col-m-4

        .. block-info:: Carlos Herranz

            .. figure:: {static}/img/carlos.jpg
                :figclass: m-portrait
                :alt: Carlos photo                         

External contributors
=====================   

NR (5G-LENA):

* Peter D. Barnes (Lawrence Livermore National Laboratory)
* Tom Henderson (University of Washington)
* Paolo Gato (Cambridge Consultants)
* Christian Mailer (Universidade Federal de Santa Catarina)
* Gideon Williams (Cambridge Consultants)
* Giovanni Grieco (Polytechnic University of Bari) (NR Tutorial)

NR-U extension:

* Getachew Redieteab (Orange)
* Christophe Delahaye (Orange)
