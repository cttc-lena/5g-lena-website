#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import shutil
import logging

AUTHOR = 'CTTC'

M_SITE_LOGO = 'https://cttc-lena.gitlab.io/5g-lena-website/static/img/5G-lena-logo.png'
M_CTTC_LOGO = 'https://cttc-lena.gitlab.io/5g-lena-website/static/img/cttc_logo_azul.jpg'
M_SITE_LOGO_TEXT = ' '

SITENAME = '5G LENA'
SITESUBTITLE = 'C++11 module to simulate 3GPP 5G networks'
SITEURL = 'https://5g-lena.cttc.es'

M_BLOG_NAME = '5G LENA Blog'
M_BLOG_URL = 'blog/'

OUTPUT_PATH = 'public/'
PATH = 'content'

STATIC_URL = 'static/{path}'
STATIC_SAVE_AS = 'static/{path}'
STATIC_PATHS = ['img', 'showcase', 'css', 'archive']
EXTRA_PATH_METADATA = {'/{static}/img/favicon.ico': {'path': '../favicon.ico'}}

ARTICLE_PATHS = ['blog']
ARTICLE_EXCLUDES = ['blog/authors', 'blog/categories', 'blog/tags']

PAGE_PATHS = ['']
PAGE_EXCLUDES = ['doc', 'img']
#READERS = {'html': None} # HTML files are only ever included from reST

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

ARCHIVES_URL = 'blog/'
ARCHIVES_SAVE_AS = 'blog/index.html'
ARTICLE_URL = 'blog/{slug}/' # category/ is part of the slug
ARTICLE_SAVE_AS = 'blog/{slug}/index.html'
DRAFT_URL = 'blog/{slug}/' # so the URL is the final one
DRAFT_SAVE_AS = 'blog/{slug}/index.html'
AUTHOR_URL = 'blog/author/{slug}/'
AUTHOR_SAVE_AS = 'blog/author/{slug}/index.html'
CATEGORY_URL = 'blog/{slug}/'
CATEGORY_SAVE_AS = 'blog/{slug}/index.html'
TAG_URL = 'blog/tag/{slug}/'
TAG_SAVE_AS = 'blog/tag/{slug}/index.html'

AUTHORS_SAVE_AS = None # Not used
CATEGORIES_SAVE_AS = None # Not used
TAGS_SAVE_AS = None # Not used

PAGINATION_PATTERNS = [(1, '{base_name}/', '{base_name}/index.html'),
                       (2, '{base_name}/{number}/', '{base_name}/{number}/index.html')]

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = 'en'

import platform
if platform.system() == 'Windows':
    DATE_FORMATS = {'en': ('usa', '%b %d, %Y')}
else:
    DATE_FORMATS = {'en': ('en_US.UTF-8', '%b %d, %Y')}

FEED_ATOM = None
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

M_LINKS_NAVBAR1 = [('Features', 'features/', 'features', [
                       ('Calibration', 'features/calibration/', 'features/calibration'),
                       ('REM maps', 'features/rem/', 'features/rem'),
                       ('PHY', 'features/phy/', 'features/phy'),
                       ('MAC', 'features/mac/', 'features/mac'),
                       ('RLC, PDCP, Core Network', 'features/core/', 'features/core')
                    ]),
                   ('Publications', 'papers/', 'papers', []),
                   ]

# Temporarily disable doxygen link until it is fixed the pages pipeline
#                   ('Docs', 'https://cttc-lena.gitlab.io/nr/html', '', [
#                     ('Getting Started', 'https://cttc-lena.gitlab.io/nr/html/index.html#getting-started', ''),
#                     ('Manual', 'https://cttc-lena.gitlab.io/nr/nrmodule.pdf', '')]

M_LINKS_NAVBAR2 = [('Blog', M_BLOG_URL, '[blog]', []),
                   ('Contact', 'contact/', 'contact', [
                       ('Team and Contributors', 'team/', 'team'),
                       ('About', 'about/', 'about'),
                   ]),
                   ('Documentation', 'https://cttc-lena.gitlab.io/nr/html/index.html#getting-started', 'https://cttc-lena.gitlab.io/nr/html/index.html#getting-started', [
                       ('Getting Started', 'https://cttc-lena.gitlab.io/nr/html/index.html#getting-started', 'https://cttc-lena.gitlab.io/nr/html/index.html#getting-started'),
                       ('Manual (HTML)', 'https://cttc-lena.gitlab.io/nr/manual/nr-module.html', 'https://cttc-lena.gitlab.io/nr/manual/nr-module.html'),
                       ('Manual (PDF)', 'https://cttc-lena.gitlab.io/nr/nrmodule.pdf','https://cttc-lena.gitlab.io/nr/nrmodule.pdf'),
                       ('Tutorial (PDF)', 'https://cttc-lena.gitlab.io/nr/cttc-nr-demo-tutorial.pdf','https://cttc-lena.gitlab.io/nr/cttc-nr-demo-tutorial.pdf'),
                       ('Docs (Doxygen)', 'https://cttc-lena.gitlab.io/nr/html', 'https://cttc-lena.gitlab.io/nr/html'),
                   ]),
                   ('Download', 'download/', 'download', []),
                   ]

#M_LINKS_FOOTER1 = [('Magnum', '/'),
#                   ('Features', 'features/'),
#                   ('Extra Functionality', 'features/extras/'),
#                   ('Plugins & Extensions', 'features/extensions/'),
#                   ('Community Contributions', 'features/community/'),
#                   ('Showcase', 'showcase/'),
#                   ('Premium Services', 'premium/'),
#                   ('Build Status', 'build-status/')]
#
#M_LINKS_FOOTER2 = [('Docs', '//doc.magnum.graphics/'),
#                   ('Getting Started', '//doc.magnum.graphics/magnum/getting-started.html'),
#                   ('Corrade', '//doc.magnum.graphics/corrade/'),
#                   ('Magnum', '//doc.magnum.graphics/magnum/'),
#                   ('Doc Downloads', 'doc-downloads/')]
#
#M_LINKS_FOOTER3 = [('Contact Us', 'contact/'),
#                   ('Blog Feed', M_BLOG_URL + '/feeds/all.atom.xml'),
#                   ('GitHub', 'https://github.com/mosra/magnum'),
#                   ('Gitter', 'https://gitter.im/mosra/magnum'),
#                   ('Twitter', 'https://twitter.com/czmosra'),
#                   ('Google Groups', 'https://groups.google.com/forum/#!forum/magnum-engine'),
#                   ('About the Project', 'about/')]

M_CSS_FILES = ['https://fonts.googleapis.com/css?family=Source+Code+Pro:400,400i,600%7CSource+Sans+Pro:400,400i,600,600i&subset=latin-ext',
               'static/m-light.css', 'js/paper-full.min.js', 'static/css/custom.css', 'static/css/youtube.css']

M_FINE_PRINT = """
| 5G-LENA. Copyright © CTTC and contributors, 2019. `Please click here to read the Legal Notice <legal>`_. Site powered by `Pelican <https://getpelican.com>`_
  and `m.css <http://mcss.mosra.cz>`_. We do not use cookies.
"""

M_ARCHIVED_ARTICLE_BADGE = """
.. note-warning:: Archived article
    This article is from {year}. While great care is taken to keep information
    up-to-date, please note that not everything in this article might reflect
    current state of the 5G-LENA project, external links might be dead and
    content might be preserved in its original form for archival purposes. Even
    the typos.
"""

M_NEWS_ON_INDEX = ("Latest news on our blog", 3)

DEFAULT_PAGINATION = 10

PLUGIN_PATHS = ['m.css/plugins/m', 'm.css/plugins']
PLUGINS = ['abbr',
           'alias',
           'code',
           'components',
           'dox',
           'filesize',
           'gh',
           'gl',
           'htmlsanity',
           'images',
           'link',
           'math',
           'metadata',
           'vk',
           'pelican_youtube']

FORMATTED_FIELDS = ['summary', 'description', 'landing', 'badge', 'header', 'footer']

THEME = 'm.css/pelican-theme/'
THEME_STATIC_DIR = 'static/'

M_THEME_COLOR = '#22272e'
M_SOCIAL_TWITTER_SITE = '@czmosra'
M_SOCIAL_TWITTER_SITE_ID = 1537427036
M_SOCIAL_IMAGE = 'static/img/site.jpg'
M_SHOW_AUTHOR_LIST = True

M_HTMLSANITY_SMART_QUOTES = True
M_HTMLSANITY_HYPHENATION = True
M_IMAGES_REQUIRE_ALT_TEXT = True
M_METADATA_AUTHOR_PATH = 'blog/authors'
M_METADATA_CATEGORY_PATH = 'blog/categories'
M_METADATA_TAG_PATH = 'blog/tags'

if not shutil.which('latex'):
    logging.warning("LaTeX not found, fallback to rendering math as code")
    M_MATH_RENDER_AS_CODE = True

DIRECT_TEMPLATES = ['archives']

SLUGIFY_SOURCE = 'basename'
PATH_METADATA = '(blog/)?(?P<slug>.+).rst'
